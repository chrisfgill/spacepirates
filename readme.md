##### get started #####
1. git clone && cd into
2. npm update
3. gulp build
4. npm start
5. browse to localhost:6063

##### aws ses #####
Currently the server relies on sending registration emails via aws

##### ./local/config.json #####
There should be a generated config file that will need information filled out
```json
{ 
    "aws_keys": ["api key","api secret"],
    "aws_region": "us-east-3",
    "mail_from": "register <register@email.com>",
    "host":"localhost",
    "port":6063
}
```
NOTE: the host portion will be the mailer confirmation uri  
There are additional non-required fields for config  
- proxy_port, the external proxy such as nginx, is use in registration emails links


##### docs, etc. #####
see more documentation [in the docs](docs) folder