##### security basics #####
The flow of security begins with registration and its out of band preshared pin  
Each connection and initial authentication is secured by the dynamic salt that is presented  
This keeps the device key secure and hidden during transmission

1. client initiate registration by specifying email
2. client keys
   - client key is resolved by server unique id and email as a hmac
   - basic random 5 digit pin
   - random device id is created from a generated uuid-v4
   - the official private device key is the combination of uid and pin as a hmac
3. email sent from server contains the client key, device id, and pin
4. client visits link in email, client decodes client key and device id from uri
5. client manually applies pin, which recreates the private device key
6. server sends a newly generated salt that is presented to each new connection
7. browser based localstorage is used for client key, device key and its id
8. client uses the salt and device key to create an hmac
9. client sends hmac to server, along with device id, and client key
10. server verifies keyed hmac with previous registration
11. if registration is occuring, device key is rehashed to prevent subsequent registrations of same keys

Steps 6-10 are rerun on each visit to site


##### missing security #####
This security model is basic and provides bare minimum for authentication of a user  
while still providing security because each connection is authenticated using a dynamic salt which is hashed  
Security flaws include both man in the middle and replay attack  
To fix either of these you should use SSL on the websocket, or HMAC and verify each transmission  
A user can still abuse their socket connection with replay attacks to submit multiple times the same action  


##### hmac why and how #####
HMAC is a keyed hash which currently suffers no known practical security flaws  
The hash used here is typically SHA1, though even MD5 in a hmac is still considered secure  
The hmac is used to both authenticate the user and authorize their actions  
A device key is the result of multiple hmac runs  

Where device-uid is a generated uuid-v4, and pin is up to a 5 digit, out of band pre-shared key,  
Server UUID is a uuid-v4 kept secret on server (not to be shared with other servers)  
and encodes the user's email for referencing. The dynamic salt is a new uuid-v4 generated each connection  
  - client key: hmac(email, server-uid)
  - registration key: hmac(device-uid, pin)
  - post-registration key: hmac(client-key, registration-key) -> tmp-key -> hmac(dynamic-salt, tmp-key)
