##### benchmarks #####

sync writes on consumer ssd:
```sh
$ npm run bench

> spacepirates@1.0.0 bench
> node build/tests/test.js

benching n = 15000
bench_write: 710.805ms
bench_read: 160.823ms
```
