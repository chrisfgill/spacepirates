##### todos #####
- create snapshot of store, for when log gets huge
- clickable menu actions on client side to represent commands
- hash out the automatic nature of game entities (eg: mining over time, and resources harvested)
- analytics of potentially abusive ip's, and block them
- ci and cd to a VM with nginx