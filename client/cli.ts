"use strict"

export class Cli {
    history_level = -1;
    history_cmd = [];

    cli_prompt;
    cli_history;
    cli_cb;
    
    ev
    silence = false  //do we print our entries?

    constructor (prompt,history,cb,ev) {
        this.cli_prompt = document.getElementById(prompt);
        this.cli_history = document.getElementById(history);
        this.cli_cb = cb;
        this.ev = ev;

        this.ev.on('cli_print', (data) => { this.print(data) });
        this.ev.on('chatmode', (mode) => {
            this.silence = mode;
            if (this.silence) {
                this.cli_history.style.display = 'none';
            }
            else {
                this.cli_history.style.display = 'initial';
            }
        });
    }

    history_apply() {
        if (this.history_level < this.history_cmd.length) {
            this.cli_prompt.value = this.history_cmd[this.history_level];
        }
    }

    submit_prompt(event) {
        if (event.key == "Enter" || event.keyCode == 13) {
            var text = this.cli_prompt.value;
            if (text == "") { return }

            this.cli_prompt.value = "";
            
            if ((this.cli_prompt.type == 'text')) {
                if (!this.silence) {
                    this.print(text)
                }

                this.history_cmd.push(text);
                this.history_level = this.history_cmd.length;
            }
            
            if (this.cli_cb) { this.cli_cb(text) }
        }
        else if (event.key == "Escape") {
            this.cli_prompt.value = "";
        }
        else if (event.key == "ArrowUp") {
            this.history_level -= 1;
            if (this.history_level < 0) {
                this.history_level = -1;
                this.cli_prompt.value = "";
            }
            else { this.history_apply(); }
        }
        else if (event.key == "ArrowDown") {
            this.history_level += 1;
            if (this.history_level == this.history_cmd.length) {
                this.cli_prompt.value = "";
            }
            else { this.history_apply(); }
        }
        else if (event.key == "Pause") { this.ev.emit('chatmode',!this.silence) }
    }

    print(text) {
        var p =  document.createElement("P");
        var t = document.createTextNode(text);
        p.appendChild(t);
        
        var tr = this.cli_history.insertRow(); // TODO: make this row clickable to reapply command
        var td = tr.insertCell();
        td.appendChild(p);

        if (this.cli_history.childNodes[0].childNodes.length > 10) {
            this.cli_history.childNodes[0].removeChild(this.cli_history.childNodes[0].childNodes[0])
        }
    }

}