"use strict"

const hmac = require('hmacsha1');
const sha1 = require('sha1');

export class Comm {
    socket = io()

    cli_conn = '>'
    cli_disc = '&#x1F419'
    cli_kind
    cli_prompt

    is_auth = false
    ev
    dsalt
    rsvp_uid
    rsvp_lkey

    chat_mode=false

    constructor (kind,input,ev) {
        this.cli_kind = document.getElementById(kind);
        this.cli_prompt = document.getElementById(input);
        this.ev = ev;

        this.ev.on('chatmode', (mode) => {
            this.chat_mode = mode;
        });

        // FIXME: losing This context
        var self = this;
        function disconnected () {
            self.cli_kind.innerHTML = self.cli_disc; 
            self.cli_prompt.style.display = 'none';
            self.is_auth = false;
            if (self.rsvp_lkey) { self.cli_prompt.type = 'number' }
            else { self.cli_prompt.type = 'email' }
        }

        disconnected();

        this.socket.on('connect', () => { // NOTE: fat arrow saves This context
            console.log('connected',this.socket.id);
            this.cli_kind.innerHTML = this.cli_conn;
            this.cli_prompt.style.display = 'block';
            this.cli_prompt.focus();
            this.is_auth = false;

            this.socket.once('salt', (salt) => {
                this.dsalt = salt.dsalt;

                if (localStorage.getItem('dkey')) {
                    this.auth(false);
                }
            });
        });

        this.socket.on('result', (data) => {
            console.log(data);
        });

        this.socket.on('chat', (data) => {
            this.ev.emit('chat',data);
        });

        this.socket.on('info', (data) => {
            console.log(data);
            this.ev.emit('info',data);
        });

        this.socket.on('disconnect', disconnected);
    }

    help () {
        this.socket.emit('?',{});
    }

    cmd (data) {
        if (this.is_auth) { 
            if (this.chat_mode) { this.socket.emit('chat',data) }
            else { this.socket.emit('cmd',data) }
        }
        else { this.auth(data) }
    }

    auth (data) {
        var self = this;

        function await_auth (d) {
            if (d.err) { 
                console.error(d.err);
                localStorage.removeItem('dkey');
                self.ev.emit('info',[d.err]);
            }
            else if (d.ok) { 
                console.log('auth res',d); 
                self.socket.removeListener('auth',await_auth);

                if (self.rsvp_lkey) {
                    var dkey = hmac(self.rsvp_lkey, localStorage.getItem('dkey'));
                    localStorage.setItem('dkey',hmac(self.dsalt,dkey));

                    document.location.href = "http://" + window.location.host;
                }

                self.is_auth = true;
                self.cli_prompt.type = 'text';
            }
        }

        

        if (!data && localStorage.getItem('dkey')) {
            this.socket.on('auth',await_auth);
            this.socket.emit('auth',
             {  uid:localStorage.getItem('uid'),
                key:hmac(this.dsalt, localStorage.getItem('dkey')),
                lkey:localStorage.getItem('lkey') })
        }
        else if (this.rsvp_uid && this.rsvp_lkey) { // key is the rsvp_uid hmac with the pin
            localStorage.setItem('uid',this.rsvp_uid);
            localStorage.setItem('lkey',this.rsvp_lkey);
            localStorage.setItem('dkey',hmac(this.rsvp_uid,data)); 
            // NOTE: never use the key 'key' with localstorage

            this.auth(false);
        }
        else if (this.rsvp_lkey) { // just the longkey? blacklist email
            this.socket.emit('blacklist',{lkey:this.rsvp_lkey});
        }
        else { //initiating registration?
            this.socket.emit('auth',{email:data});
        }
    }
}