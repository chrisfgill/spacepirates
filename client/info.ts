"use strict";

export class Info {
    info_div;
    ev;

    constructor (div, ev) {
        this.info_div = document.getElementById(div);
        this.ev = ev;

        this.ev.on('info', (l) => { this.info_fill(l) });
    }

    // TODO: template this
    info_fill (list: string[]) {
        this.info_div.innerHTML = "";
        let tbl  = document.createElement('table');
        var p,text;
        var tr,td;

        list.forEach(e => {
            p =  document.createElement("P");
            text = document.createTextNode(e);
            p.appendChild(text);
            
            tr = tbl.insertRow();
            td = tr.insertCell();
            td.appendChild(p);
            
        });

        this.info_div.appendChild(tbl);
    }
}