"use strict";

import modComm = require ('./comm');
import modCli = require('./cli');
import modInfo = require('./info');

const emitter = require('events');
const base64 = require('base-64');
const utf8 = require('utf8');

export class Client {
    ev = new emitter;
    info;
    comm;
    cli;

    chat_mode = false

    constructor ({info,kind,prompt,cli_history,chat_history}) {
        this.info = new modInfo.Info(info,this.ev);
        this.comm = new modComm.Comm(kind,prompt,this.ev); 

        this.cli = new modCli.Cli(prompt,cli_history, (data) => { this.comm.cmd(data) },this.ev );

        let chat_ele = document.getElementById(chat_history) as any;
        let cli_ele = document.getElementById(cli_history) as any;
        this.ev.on('chat', (data) => {
            var p =  document.createElement("P");
            var t = document.createTextNode(data);
            p.appendChild(t);
            
            var tr = chat_ele.insertRow(); // TODO: make this row clickable to reapply command
            var td = tr.insertCell();
            td.appendChild(p);

            if (chat_ele.childNodes[0].childNodes.length > 10) {
                chat_ele.childNodes[0].removeChild(chat_ele.childNodes[0].childNodes[0])
            }
        });

        let prompt_kind = document.getElementById(kind);
        this.ev.on('chatmode', (mode) => {
            if (!prompt_kind.classList.contains('cmd')) {
                if(!mode) { prompt_kind.classList.add("cmd") }
            }
            else {
                prompt_kind.classList.toggle("cmd");
            }

            if (mode) {
                chat_ele.style.display = 'initial';
            }
            else {
                chat_ele.style.display = 'none';
            }
        });

        this.mode();


        let split = window.location.href.toString().split('#');
        if (split.length > 1) {
            this.ev.emit('info',['enter pin']);
            let bytes = base64.decode(split[1]);
            let text = utf8.decode(bytes);
            let uri = text.split('#');
            let rsvp_lkey = uri[0];
            let rsvp_uid = uri[1];
            
            if (rsvp_lkey && rsvp_uid) { 
                this.comm.rsvp_uid = rsvp_uid;
                this.comm.rsvp_lkey = rsvp_lkey;
            }
            else if (rsvp_lkey) {
                this.ev.emit('info',['submitting blacklist']);
                this.comm.rsvp_lkey = rsvp_lkey;
                this.comm.auth(false);
            }
        }
    }

    mode() {
        this.chat_mode = !this.chat_mode;
        this.ev.emit('chatmode',this.chat_mode)
    }

    accel () {
        var self = this;
        function accelUpdate(e) {
            var ax = e.accelerationIncludingGravity.x;
            var ay = e.accelerationIncludingGravity.y;
            var az = e.accelerationIncludingGravity.z;
            
            var xp = Math.atan2(ay, az);
            var yp = Math.atan2(ax, az);

            //console.log(xp,yp);
            //self.ev.emit('info',[ax]);
        }

        window.addEventListener("devicemotion", accelUpdate, true);
    }
}

// TODO: get module exports working in standlone for browserify
global.window.Client = Client;