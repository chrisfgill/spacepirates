"use strict";

import fs = require('fs');
const hmac = require('hmacsha1');
const sha1 = require('sha1');
const uuid = require('node-uuid');


export class Auth {
    salt:string
    rsvp = {}

    constructor (salt) {
        this.salt = salt;
    }

    register (email:string): string[] {
        let pin = this.gen_pin().toString();
        let uid = this.gen_uid().toString();
        let key = hmac(uid,pin);
        let lkey = hmac(email, this.salt); //long key, never should change
        this.rsvp[lkey] = key;
        return [lkey,uid,pin]
    }

    /// compares a static key with a dynamic key
    auth (dkey:string, key:string, dsalt:string): boolean {
        if (hmac(dsalt,key) === dkey) { 
            console.log('login',key);
            return true
        }
        else { return false }
    }

    gen_hmac(d,k) {
        return hmac(d,k)
    }
    gen_uid() {
        return uuid.v4()
    }

    gen_pin() {
        return Math.floor(Math.random() * 99999)
    }
}