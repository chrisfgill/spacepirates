"use strict"

import fs = require('fs');

export function init(ev) {
    let cmds_file = process.cwd() + '/game/cmds.json';
    
    function set_cmds(data: string) {
        ev.emit('commands',JSON.parse(data));
    }

    read_file_json(cmds_file,set_cmds);
    watch_file_json(cmds_file,set_cmds);
}

// TODO: build this up as an async process
function read_file_json(f,cb) {
    fs.readFile(f, {encoding: 'utf-8', flag: 'rs'}, function(e, data) {
        if (data) {
            console.log('updating commands');
            cb(data);
        }
    });
}

function watch_file_json(f,cb) {
    fs.watchFile(f, (e, fn) => { //NOTE: watchFile is slower than watch, but fires only once
        if (fn) read_file_json(f,cb);
    });
}