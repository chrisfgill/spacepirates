"use strict";

import {EventEmitter} from 'events';
import fs = require('fs');
import {Auth} from './auth';

export class Store {
    players = {}
    blacklist = []
    entities = {}

    path
    writer: fs.WriteStream
    loaded = false
    ev: EventEmitter = new EventEmitter();

    constructor (config) {
        this.ev.on('begin', () => {
            if (config.path) { this.path = config.path }
            else { this.path = process.cwd()+'/local/store.log' }
            
            let reader;
            if (config.reader) { reader = config.reader }
            else { reader = fs.createReadStream(this.path) }

            let rl = require('readline').createInterface({input:reader});
            rl.on('line', (line) => {
                let actions = JSON.parse(line);
                for (var x in actions) { // likely only 1 action
                    let act = x.toString();
                    this.ev.emit(act,actions[x]);
                }
            });

            rl.on('close', () => {
                this.loaded = true;
                this.ev.emit('store_loaded');
            });

            this.ev.once('store_loaded', () => {
                this.writer = fs.createWriteStream(this.path, {'flags': 'a'});
                
                this.ev.on('snapshot', () => {
                    let tmp = this;
                    tmp.ev = undefined;
                    tmp.writer = undefined;

                    let data = JSON.stringify(tmp);
                    fs.writeFile(this.path+'.snap', data, (err) => {
                        if (err) console.error(err)
                    });
                });

                this.ev.on('store', (act) => {
                    let s = JSON.stringify(act.arg); //NOTE: does this first, otherwise async blues occur
                    s = '{"' + act.action + '":' + s + '}\n';
                    if (!this.writer.write(s, act.cb)) {
                        console.error('high watermark on writestream for store');
                        this.writer.once('drain',
                                         () => { //backpressured
                                             this.ev.emit(act.action,act.arg)
                                         })
                    }
                    else { this.ev.emit(act.action,act.arg) }
                });
            });
        });
    }

    emit (action:string, arg, cb?) {
        this.ev.emit('store',{action,arg,cb});
    }
}
