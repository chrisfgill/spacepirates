"use strict";

import express = require('express');
import http = require('http');
import io = require('socket.io');

import {Store} from './store';
import {Player} from './player';
import {Auth} from './auth';
import {Actions} from './actions';
import {Entity} from './entity';

export class Server {
    config

    auth: Auth
    store: Store
    app = express()
    server = (http as any).Server(this.app); //ignores ts errors!
    ws = io(this.server);

    commands = {};
    connections = {};

    constructor (config) {
        this.config = config;
        this.server.listen(config.port);
        config.events.on('commands', (data) => { this.commands = data });

        config.events.once('store', (data) => {
            if (data.loaded) {
                this.ws.on('connection', (socket) => { this.handle(socket) });
            }
        });

        this.auth = new Auth(config.uuid);
        this.store = new Store({events:config.events});
        Actions(this.store);
    }

    handle (socket) {
        socket.join('world');

        let auth_client = false;
        let ip = socket.handshake.headers['x-forwarded-for'];
        if (!ip) { ip = socket.handshake.address }
        console.log(ip,socket.id);
        const dsalt = this.auth.gen_uid(); // gen a uid for connection

        socket.emit('salt', {dsalt:dsalt} );

        var self = this;
        function handle_reg (data) {
            if (data.email) {
                console.log('register',data.email)
                let [lkey,uid,pin] = self.auth.register(data.email);

                if (!self.store.blacklist[lkey]) {
                    self.config.mailer.send_reg(data.email,lkey,uid,pin);
                    socket.emit('info',['check your email','close this tab'])
                }
                else {
                    console.log('blacklisted',data.email); 
                    socket.emit('info',['email blacklisted']);
                    socket.disconnect();
                }
            }
        }

        function handle_auth (data) {
            if (data.lkey && data.key && data.uid) {
                let p = self.store.players[data.lkey];

                if (p && p.keys[data.uid]) {
                    if (self.auth.auth(data.key, p.keys[data.uid], dsalt))  { 
                        //authorized/validated player?
                        socket.emit('auth',{ok:true});
                        handle_when_auth();
                    }
                    else { 
                        socket.emit('auth',{err:'invalid login'});
                        console.log('invalid auth',data.lkey)
                    }
                }
                else if (self.auth.rsvp[data.lkey]) { // finalize reg of new user
                    if (self.auth.auth(data.key, self.auth.rsvp[data.lkey], dsalt)) {
                        data.skey = self.auth.rsvp[data.lkey];
                        data.skey = self.auth.gen_hmac(data.lkey,data.skey);
                        data.skey = self.auth.gen_hmac(dsalt,data.skey);

                        self.auth.rsvp[data.lkey] = null;

                        let and_auth = () => {
                            self.store.emit('player_push_key', data, () => {
                                socket.emit('auth', {ok:true, key:self.auth.rsvp[data.lkey]});
                                handle_when_auth();
                            });
                        };

                        if (!self.store.players[data.lkey]) {
                            self.store.emit('player_add', new Player(data.lkey), 
                                () => {
                                    let ent = new Entity(data.lkey);
                                    self.store.emit('entity_new',ent);
                                    and_auth() 
                                });
                        }
                        else { and_auth() }
                    }
                    else { 
                        socket.emit('auth',{err:'invalid login'});
                        console.log('invalid auth',data.lkey)
                    }
                }
                else { 
                    socket.emit('auth',{err:'invalid login'});
                    socket.disconnect() 
                }            
            }
            else if (data.email) { // start reg of new user
                handle_reg(data)
            }
            else { 
                socket.emit('auth',{err:'invalid login'});
                socket.disconnect() 
            }
        }

        function handle_when_auth () {
            socket.removeListener('auth',handle_auth);
            socket.emit('info',['welcome']);
            auth_client = true;

            socket.on('?', (data) => {
                socket.emit('result',self.commands);
            });
            socket.on('cmd', (data) => {
                console.log(data);
                if (self.commands[data]) { console.log('found cmd'); }
            });

            socket.on('chat',(text) => {
                self.ws.to('world').emit('chat',text);
            });
        }

        socket.on('auth', handle_auth);

        socket.on('blacklist',(data) => {
            if (data.lkey) {
                this.store.emit('blacklist',data.lkey)
                socket.emit('info',['email blacklisted']);
                socket.disconnect()
            }
        });
    }
}