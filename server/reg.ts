const sesMailer = require('amazon-ses-mailer');
const base64 = require('base-64');
const utf8 = require('utf8');

export class Mailer {
    mailer
    mail_from
    host
    port

    constructor (config:
        {   host:string, 
            port:number,
            proxy_port?:number,
            mail_from:string, 
            aws_keys: [string], 
            aws_region: string 
        }) {

        this.mailer = new sesMailer(config.aws_keys[0], config.aws_keys[1], config.aws_region);
        this.mail_from = config.mail_from;
        this.host = config.host;
        if (config.proxy_port) { this.port = config.proxy_port }
        else { this.port = config.port }
    }

    send (mail: {to: string[], subject:string, body:{text?:string, html?:string}}) {
        if ((mail.body.text) || (mail.body.html)) {
            this.mailer.send({from: this.mail_from, to: mail.to,
                subject: mail.subject,
                body: mail.body,
            });
        }
        else { console.error('needs body elements for mail') }
    }

    send_reg(email:string,lkey:string,uid:number,pin:number) {
        let bytes = utf8.encode(lkey + "#" + uid);
        let enc = base64.encode(bytes);

        let port = "";
        if (this.port != 80) { port = ":"+this.port }

        // TODO: template this mailer
        this.send({  
            to:[email], // TODO: verify email is an actual email
            subject:'Registration completion required',
            body:{html:
                "<h4>Register your account with "+this.host+"</h4>"+
                "<p>"+
                    "<a href='http://"+this.host+ port + "/#" + enc +"'>" +
                        "Click the link to complete your registration" + 
                    "</a>"+
                "</p>"+
                "<p>and enter in this pin: <i>"+pin+"</i></p>"+
                "<p style='margin-top:100px'>"+
                    "<a href='http://"+this.host+":"+this.port + "/#" +
                        base64.encode(utf8.encode(lkey)) + "'>" +
                        "click to block my email from further registration requests" +
                    "</a>" +
                "</p>"
            }
        });
        console.log('sending registration email',email);
    }
}