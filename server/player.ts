export class Player {
    name: string
    date: number
    lkey: string
    keys: {} = {}
    inv: string[] = []

    constructor (lkey:string) {
        this.name = "Player_" + Math.floor(Math.random() * 99999).toString();
        this.lkey = lkey;
        this.date = Date.now();
    }
}