import {Store} from './store';

export function Actions (store:Store) {
    let ev = store.ev;

    ev.on('player_add', (player) => {
        store.players[player.lkey] = player;
    });

    ev.on('player_push_key', (d) => {
        store.players[d.lkey].keys[d.uid] = d.skey;
    });

    ev.on('player_rename', (lkey,name) => {
        store.players[lkey].name = name;
    });

    ev.on('blacklist', (lkey) => {
        store.blacklist[lkey] = true;
    });

    ev.on('entity_new', (ent) => {
        store.entities[ent.name] = ent;
        store.players[ent.owner].inv.push(ent.name);
    });

    ev.emit('begin')
}