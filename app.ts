"use strict"

import {Server} from './server/main';
import express = require('express');

const emitter = require('events');
let ev = new emitter;

const server_uid = require(process.cwd()+'/local/uuid.json');
console.log('server uid',server_uid);

const config = require(process.cwd()+'/local/config.json');
config.uuid = server_uid;
config.events = ev;

import {Mailer} from './server/reg';
let mailer = new Mailer(config);
config.mailer = mailer;

const server = new Server(config);

server.app.get('/', function (req, res) {
  res.sendFile(__dirname + '/client/deps/index.html');
});

server.app.use("/", express.static(__dirname + '/client/'));

import cmd = require('./server/cmd');
cmd.init(ev);

