import fs = require('fs');
const emitter = require('events');

import {Store} from '../server/store';
import {Actions} from '../server/actions';
import {Player} from '../server/player';
import {Entity} from '../server/entity';
import uuid = require('node-uuid');

export class Bench {
    path = './build/tests/test-store.log'
    store:Store
    n:number
    events = new emitter;

    constructor(n:number) {
        fs.writeFileSync(this.path, ''); // rewrite file

        // NOTE: for benching WRITE then store loads once at first, it's empty
        this.store = new Store({path:this.path,events:this.events});
        Actions(this.store);

        this.n = n;
    }

    private bench_write() {
        this.n -= 1;
        if (this.n > -1) { 
            let owner = uuid.v4().toString();
            this.store.emit('player_add', new Player(owner), 
                () => {
                    this.store.emit('entity_new', new Entity(owner),
                        () => {
                            this.bench_write()
                        });
                });    
        }
        else {
            this.store.writer.end(() => {
                console.timeEnd('bench_write');
                this.events.emit('bench_write')
            });
        }
    }

    // NOTE: bench_write runs first
    private bench_read() {
        console.time('bench_read');
        this.store = new Store({path:this.path,events:this.events});
        Actions(this.store);
        this.store.ev.once('store_loaded', () => {
            console.timeEnd('bench_read');
            this.events.emit('bench_read')
        });
    }

    bench(cb?: (Store) => void) {
        this.store.ev.once('store_loaded', () => { //wait for store to be live
            this.events.once('bench_write', () => {
                this.events.once('bench_read', () => {
                    if (cb) cb(this.store)
                });
                this.bench_read()
            });

            console.log('benching n =',this.n);
            console.time('bench_write');
            this.bench_write();
        });
    }
}
