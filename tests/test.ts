import {Bench} from './bench';
import {Store} from '../server/store';

import chai = require('chai');

let n = 5000;
let b = new Bench(n);
b.bench((store:Store) => {
    chai.assert.lengthOf(Object.keys(store.players), n);
    //store.ev.once('loaded',() => {
        chai.assert.lengthOf(Object.keys(store.entities), n);
    //});
});
