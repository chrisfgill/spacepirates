var gulp = require('gulp');
var ts = require('gulp-typescript');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var clean = require('gulp-clean');
var tsify = require('tsify');

var uuid = require('node-uuid');
var fs = require('fs');

gulp.task('default', ['build'], function() {
    gulp.watch('server/*.ts', ['typescript']);
    gulp.watch('tests/*.ts', ['typescript']);
    gulp.watch('client/*.ts', ['browserify']);
    gulp.watch('client/deps/*', ['deps-client']);
    gulp.watch('server/deps/*', ['deps-server']);
    gulp.watch('app.ts', ['typescript']);
});

gulp.task('typescript', function() {
    var tsProject = ts.createProject('tsconfig.json');
    tsProject.config.exclude.push("client");
    var tsResult = tsProject.src()
		.pipe(ts(tsProject));
	
	return tsResult.js.pipe(gulp.dest('build'));
});

gulp.task('browserify', ['typescript_client'], function () {
    return browserify('build/tmp/main.js')
            .bundle()
            .on('error', function (e) { console.error(e.toString()) })
            .pipe(source('bundle.js'))
            .pipe(gulp.dest('build/client')); //process.stdout)//
});

gulp.task('typescript_client', function() {
    var tsProject = ts.createProject('tsconfig.json');
    tsProject.config.exclude.push("server");
    tsProject.config.exclude.push("app.ts");
    var tsResult = tsProject.src()
		.pipe(ts(tsProject));
	
	return tsResult.js.pipe(gulp.dest('build/tmp'));
});

gulp.task('deps-server', function() {
    return gulp.src('server/deps/**/*.*')
        .pipe(gulp.dest("build/server/deps"));
});

gulp.task('deps-client', function() {
    return gulp.src('client/deps/**/*.*')
        .pipe(gulp.dest("build/client/deps"));
});

/// generate game assets
gulp.task('gen-game', function() {
    let path = './local/uuid.json';
    fs.access(path, fs.F_OK, (err) => {
        if (err) {
            let id = {uuid:uuid.v4()};
            fs.writeFile(path, JSON.stringify(id), (err) => {
                if (err) { console.error(err) }
            })
        }
    });

    path = './local/store.log';
    fs.access(path, fs.F_OK, (err) => {
        if (err) {
            fs.writeFile(path, '', (err) => {
                if (err) { console.error(err) }
            })
        }
    });

    path = './local/config.json';
    fs.access(path, fs.F_OK, (err) => {
        if (err) {
            let config = { 
                "aws_keys": ["api","key"],
                "aws_region": "us-west-2",
                "mail_from": "email <email@email>",
                "host":"localhost",
                "port":6063
            };
            fs.writeFile(path, JSON.stringify(config), (err) => {
                if (err) { console.error(err) }
            })
        }
    });
});

gulp.task('build', ['gen-game','browserify','typescript','deps-server','deps-client']);